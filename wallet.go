package wallet

import (
	"encoding/json"
	"log"
	"os"
)

func LoadConfiguration(file string) []*Wallet {
	var wallets []*Wallet
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		log.Fatalln(err.Error())
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&wallets)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return wallets
}

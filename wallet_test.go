package wallet

import (
	crand "crypto/rand"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"math/rand"
	"testing"
)

var (
	accountSet     uint64
	transactionSet int
	allWallets     []*Wallet
	w              *Wallet
	i64            int64
	bStr           []byte
	reason         string
	valid          bool
	validErr       error
	err            error
)

func init() {
	flag.Uint64Var(&accountSet, "account", 10000, "The Account that should be used default: 10000")
	flag.IntVar(&transactionSet, "txcount", 100, "The Count of Transactions that should be used default: 100")

	flag.Parse()

	allWallets = LoadConfiguration("accounts.json")

	for _, w = range allWallets {
		if w.ID == uint64(accountSet) {
			log.Printf("Wallet Start Balance: %d -> %.2f\n", w.GetBalance(), (float64(w.GetBalance()) / 100))
			break
		}
	}

	if w.ID != accountSet {
		log.Fatalf("Account %d not found.\n", accountSet)
		return
	}
}

func TestCreateWallet(t *testing.T) {
	var aId = uint64(100003)
	w2 := &Wallet{}
	w2.ID = aId
	allWallets = append(allWallets, w2)

	for _, w2 = range allWallets {
		if w2.ID == aId {
			log.Printf("Wallet Start Balance for %d: %d -> %.2f\n", w2.ID, w2.GetBalance(), (float64(w2.GetBalance()) / 100))
			break
		}
	}

	if w2.ID != aId {
		t.Fatalf("Account %d not found.\n", accountSet)
	}
	return
}

func TestTransactions(t *testing.T) {
	valid, validErr = w.VerifyBalanceHistory()

	fmt.Printf("History Valide: %v - %v\n", valid, validErr)

	for i := 0; i < transactionSet; i++ {
		// create an amount between 1.00 and 500.00 as int64
		i64 = int64(randomInt(100, 50000))
		if i%2 == 0 {
			reason = fmt.Sprintf("Deposit Money: %d -> %.2f", i64, (float64(i64) / 100))
		} else {
			i64 = i64 * -1
			reason = fmt.Sprintf("Withdraw Money: %d -> %.2f", i64, (float64(i64) / 100))
		}

		log.Println(reason)
		err = w.Transaction(i64, reason)

		if err != nil {
			log.Print(fmt.Sprintf("Error Transaction: %v\nTransaction not processed\n", err))
		}

		log.Printf("New Balance: %d -> %.2f", w.GetBalance(), (float64(w.GetBalance()) / 100))
		//time.Sleep(sleepDuration)
	}

	valid, validErr = w.VerifyBalanceHistory()

	log.Printf("Final Balance: %d -> %.2f\nHistory Valide: %v - %v\n", w.GetBalance(), (float64(w.GetBalance()) / 100), valid, validErr)

}

func TestWriteAccountsJSON(t *testing.T) {
	bStr, err = json.MarshalIndent(allWallets, "", "\t")
	if err != nil {
		fmt.Printf("JSON Marshal Error: %v\n", err)
	}

	err = ioutil.WriteFile("output.json", bStr, 0644)
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
}

func randomInt(min, max int) int {
	randomSeed, _ := crand.Int(crand.Reader, big.NewInt(9223372036854775807))
	rand.Seed(randomSeed.Int64())
	return rand.Intn(max-min) + min
}

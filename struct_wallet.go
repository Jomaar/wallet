package wallet

import (
	"errors"
	"fmt"
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
)

// Wallet is the structure of the virtual wallet
type Wallet struct {
	ID      uint64           `json:"ID"`
	Balance int64            `json:"Balance"`
	History []*WalletHistory `json:"History"`
	rwmu    sync.RWMutex
}

// Transaction creates a transaction that increase or decrease the Balance
// it also adds a history entry
// if the current Balance is lower then the withdraw it returns an error message
// else it will return nil
func (w *Wallet) Transaction(amount int64, reason string) error {
	w.rwmu.RLock()
	var changeBalance int64
	var currentBalance = w.getBalance()
	w.rwmu.RUnlock()

	if changeBalance = currentBalance + amount; changeBalance >= 0 {
		wh := WalletHistory{
			ID:     uuid.NewV4().String(),
			Value:  amount,
			Reason: reason,
			Date:   time.Now().UTC(),
		}
		w.rwmu.Lock()
		w.History = append(w.History, &wh)
		w.Balance = changeBalance
		w.rwmu.Unlock()
		return nil
	}

	return errors.New(fmt.Sprintf("Not enough Balance: %d -> %.2f", changeBalance, (float64(changeBalance) / 100)))
}

// GetBalance returns the current balance
func (w *Wallet) GetBalance() int64 {
	return w.getBalance()
}

// getBalance returns the current balance
// it will be used internal
func (w *Wallet) getBalance() int64 {
	w.rwmu.RLock()
	defer w.rwmu.RUnlock()
	return w.Balance
}

// GetHistory returns the wallet history
func (w *Wallet) GetHistory() []*WalletHistory {
	return w.getHistory()
}

// getHistory returns the wallet history
// it will be used internal
func (w *Wallet) getHistory() []*WalletHistory {
	w.rwmu.RLock()
	defer w.rwmu.RUnlock()
	return w.History
}

// VerifyBalanceHistory checks if the history match the current w.Balance
// it returns true and a nil error if the check pass
// if the check failed it returns false and returns an error message
func (w *Wallet) VerifyBalanceHistory() (bool, error) {
	var allTransBalance int64

	w.rwmu.RLock()
	var currentBalance = w.getBalance()
	var history = w.getHistory()
	w.rwmu.RUnlock()

	for _, transaction := range history {
		allTransBalance += transaction.Value
	}
	if allTransBalance != currentBalance {
		m := fmt.Sprintf("%d != %d", allTransBalance, currentBalance)
		return false, errors.New("Transaction History mismatch current Balance: " + m)
	}

	return true, nil
}

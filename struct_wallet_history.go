package wallet

import (
	"time"
)

type WalletHistory struct {
	ID     string    `json:"ID"` // uuid or something similar
	Value  int64     `json:"Value"`
	Reason string    `json:"Reason"`
	Date   time.Time `json:"Date"`
}
